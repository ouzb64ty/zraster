#include "main.h"

static void
sdl_init(t_app *app) {

  int rendererFlags = SDL_RENDERER_ACCELERATED;

  if (SDL_Init(SDL_INIT_VIDEO) < 0)
    return ;
  app->window = SDL_CreateWindow(
      "SDL Template",
      SDL_WINDOWPOS_CENTERED,
      SDL_WINDOWPOS_CENTERED,
      SCREEN_WIDTH,
      SCREEN_HEIGHT,
      SDL_WINDOW_SHOWN);
  if (app->window == NULL) {
    return ;
  }
  app->renderer = SDL_CreateRenderer(app->window, -1, rendererFlags);
  if (app->renderer == NULL) {
    return ;
  }
}

static void
sdl_event(t_app *app) {

  SDL_Event event;

  (void)app;
  while (SDL_PollEvent(&event)) {
    if (event.type == SDL_QUIT) {
      exit(0);
    } else if (event.type == SDL_KEYDOWN) {
      switch (event.key.keysym.sym) {
        case SDLK_LEFT:
          break ;
        case SDLK_RIGHT:
          break ;
        case SDLK_UP:
          break ;
        case SDLK_DOWN:
          break ;
      }
    }
  }
}

static void
sdl_render(t_app *app) {

  SDL_RenderPresent(app->renderer);
}

static void
sdl_prepare(t_app *app) {

  (void)app;
  return ;
}

static int
sdl_core(void) {

  t_app app;
  int current_time = 0;
  int previous_time = 0;

  memset(&app, 0, sizeof(t_app));
  sdl_init(&app);
  if (app.window == NULL || app.renderer == NULL)
    return -1;
  while (1) {
    previous_time = SDL_GetTicks();
    sdl_prepare(&app);
    sdl_event(&app);
    sdl_render(&app);
    current_time = SDL_GetTicks();
    if (current_time - previous_time < MILLISECONDS_PER_FRAME)
      SDL_Delay(MILLISECONDS_PER_FRAME - (current_time - previous_time));
  }
  return 1;
}

int
main(int argc, char *argv[]) {

  int ret = 0;

  ret = sdl_core();
  if (ret == -1)
    return 1;
  return 0;
}
