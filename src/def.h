#ifndef DEF_H
# define DEF_H

# include <SDL2/SDL.h>

typedef enum e_bool {
  TRUE,
  FALSE
} t_bool;

# define MSG_USAGE "usage: ./sdl-template\n"

/* Configuration */

# define SCREEN_WIDTH 500
# define SCREEN_HEIGHT 500
# define FPS 60
# define MILLISECONDS_PER_FRAME (1000 / FPS)

typedef struct s_app {
  SDL_Renderer *renderer;
  SDL_Window *window;
} t_app;

typedef struct s_point {
  int x;
  int y;
} t_point;

#endif
